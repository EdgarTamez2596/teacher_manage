
const Students = require("../models/students.model.js");

exports.addStudents = (req, res) =>{
  // Validacion de request
  if(!req.body){    
    res.status(400).send({
        message:"El Contenido no puede estar vacio"
    });
	}

  // INSERTAR ESCULAS DEL USUARIO
  for(const i in req.body){
		
    Students.addStudent(req.params.group_id,req.body[i], (err,data)=>{
			if(err){
				res.status(500).send({
					message:
					err.message || "Se produjo algún error al crear el Estudiante"
				})
				return;
			}
		})
	};
	
	res.send("El grupo se ha creado correctamente.")
	
};

// Find a single users with a usersId
exports.studentGroup = (req, res) => {
	Students.studentxGroup(req.params.group_id, (err, data) => {
	  if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
				message: `No encontre los estudiantes con el id ${ req.params.group_id }.`
				});
			} else {
				res.status(500).send({
				message: "Error al recuperar los estudiantes con el id" + req.params.group_id 
				});
			}
	  } else res.send(data);
	});
};

exports.updateStudents = (req, res) =>{
	// Validacion de request
	if(!req.body){    
		res.status(400).send({
				message:"El Contenido no puede estar vacio"
		});
	}

	// ELIMINAR ALUMNO
	for(const i in req.body){
		Students.deleteStudent(req.body[i].id, (err,data)=>{
			if(err){
				res.status(500).send({
					message:
					err.message || "Se produjo algún error al crear el Estudiante"
				})
				return;
			}
		});
	};

	// INSERTAR ESCULAS DEL USUARIO
	for(const i in req.body){
		Students.addStudent(req.params.group_id,req.body[i], (err,data)=>{
			if(err){
				res.status(500).send({
					message:
					err.message || "Se produjo algún error al crear el Estudiante"
				})
				return;
			}
		});
	};
	
	res.send("El grupo se ha actualizado correctamente.")
	
};


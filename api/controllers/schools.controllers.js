
const Schools = require("../models/schools.model.js");

exports.findAll = (req, res) => {
  Schools.getAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los usuarios"
      });
    else res.send(data);
  });
};



const Groups = require("../models/groups.model.js");

// Crear y salvar un nuevo usuario
exports.create = (req, res) => {
	// Validacion de request
  if(!req.body){
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }

  // Guardar el CLiente en la BD
  Groups.create(req.body, (err, data)=>{
  	// EVALUO QUE NO EXISTA UN ERROR
  	if(err)
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el Grupo"
  		})
  	else res.send(data)

  })
};

exports.schoolbyId = (req, res)=>{
		console.log('schoolxId', req.body)
    Groups.schoolxId(req.body,(err,data)=>{
			if(err)
				res.status(500).send({
					message:
						err.message || "Se produjo algún erro al recuperar los grupos por escuelas."
				});
				else res.send(data);
    });
};

exports.findAll = (req,res) => {
    Groups.getAll((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Se produjo algún error al recuperar los grupos"
        });
      else res.send(data);
    });
};

exports.groupsOne = (req, res) => {
	Groups.groupsxId(req.params.group_id, (err, data) => {
	  if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
				message: `No encontre el grupo con el id ${ req.params.group_id }.`
				});
			} else {
				res.status(500).send({
				message: "Error al recuperar los estudiantes con el id" + req.params.group_id 
				});
			}
	  } else res.send(data);
	});
};

exports.updateGroups = (req, res) =>{
	if (!req.body) {
		res.status(400).send({
		  message: "El Contenido no puede estar vacio!"
		});
	}
	
	Groups.putGroup(req.params.group_id, req.body ,(err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
				message: `No encontre el grupo con el id ${req.params.group_id }.`
				});
			} else {
				res.status(500).send({
				message: "Error al actualizar el grupo con el id" + req.params.group_id 
				});
			}
		} 
		else res.send(data);
	}
	);
}








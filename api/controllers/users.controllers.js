
const Users = require("../models/users.model.js");

exports.session = (req, res)=>{
  Users.login(req.body,(err, data)=>{
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Este cliente no se encuentra registrado`
        });
      } else {
        res.status(500).send({
          message: "Error al buscar el usuario" 
        });
      }
    } else 
    res.send(data);
  });
};

// PROCESO PARA ACTUALIZAR PERFIL
exports.updatexId = (req, res) =>{
  Users.updatexId(req.params.userId, req.body, (err,data)=>{
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre el usuario con el id ${req.params.userId }.`
        });
      } else {
        res.status(500).send({
          message: "Error al actualizar el usuario con el id" + req.params.userId 
        });
      }
    }else res.send(data);
  })
  // ELIMINAR ESCUELAS DEL USUARIO
  Users.deleteSchoolsxUser(req.params.userId,(err, data)=>{
    if (err)
      console.log('Este usuario no tiene escuelas registradas')
  });

  // INSERTAR ESCULAS DEL USUARIO
  for(const i in req.body.user_schools){
    Users.createSchoolsxUser(req.params.userId,req.body.user_schools[i])
  }
};

// Crear y salvar un nuevo usuario
exports.create = (req, res) => {
	
  if(!req.body){ // Validacion de request
  	res.status(400).send({
  		message:"El Contenido no puede estar vacio"
  	});
  }
  
  const user = new User({ // Crear un usuario
  	email  : req.body.email,
  	name   : req.body.name,
  	active : req.body.active
  })
  
  User.create(user, (err, data)=>{ // Guardar el CLiente en la BD
  	
  	if(err) // EVALUO QUE NO EXISTA UN ERROR
  		res.status(500).send({
  			message:
  			err.message || "Se produjo algún error al crear el Usuario"
  		})
  	else res.send(data)

  })
};

exports.findAll = (req, res) => { // Retrieve all userss from the database.
  Users.getAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al recuperar los usuarios"
      });
    else res.send(data);
  });
};

// Find a single users with a usersId
exports.findOne = (req, res) => {
  Users.findById(req.params.userId, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `No encontre el cliente con el id ${ req.params.userId }.`
        });
      } else {
        res.status(500).send({
          message: "Error al recuperar el usuario con el id" + req.params.userId 
        });
      }
    } else res.send(data);
  });
};

// Update a users identified by the usersId in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body) {
    res.status(400).send({
      message: "El Contenido no puede estar vacio!"
    });
  }

  Users.updateById(req.params.userId, new User(req.body),(err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `No encontre el usuario con el id ${req.params.userId }.`
          });
        } else {
          res.status(500).send({
            message: "Error al actualizar el usuario con el id" + req.params.userId 
          });
        }
      } 
      else res.send(data);
    }
  );
};

// Delete a users with the specified usersId in the request
exports.delete = (req, res) => {
  Users.remove(req.params.usersId, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Users with id ${req.params.usersId}.`
        });
      } else {
        res.status(500).send({
          message: "No encontre el usuario con el id " + req.params.usersId
        });
      }
    } else res.send({ message: `El usuario se elimino correctamente!` });
  });
};

// Delete all userss from the database.
exports.deleteAll = (req, res) => {
  Users.removeAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Se produjo algún error al eliminar a todos los clientes."
      });
    else res.send({ message: `Todos los usuarios se eliminarion correctamente!` });
  });
};

// module.exports = {

// }
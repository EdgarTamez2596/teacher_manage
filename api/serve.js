// IMPORTAR DEPENDENCIAS
const express    = require("express");
const bodyParser = require("body-parser");
var   cors       = require('cors')
// IMPORTAR EXPRESS
const app = express();
// IMPORTAR PERMISOS
app.use(cors())
// parse requests of content-type: application/json
app.use(bodyParser.json());
// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// ----IMPORTAR RUTAS---------------------------------------->
var r_users   = require('./routes/users.routes');
var r_schools = require('./routes/schools.routes');
var r_groups = require('./routes/groups.routes');
var r_students = require('./routes/students.routes');

r_users(app);
r_schools(app);
r_groups(app);
r_students(app);
// ----FIN-DE-LAS-RUTAS-------------------------------------->

// DEFINIT PUERTO EN EL QUE SE ESCUCHARA
app.listen(3000, () => {
  console.log("|****************** S-O-F-S-O-L-U-T-I-O-N *********************| ");
  console.log("|******************** C-R-E-A-T-I-B-E *************************| ");
  console.log("|**************************************************************| ");
  console.log("|************ Servidor Corriendo en el Puerto 3000 ************| ");
});
module.exports = app => {
  const users = require('../controllers/users.controllers') // --> ADDED THIS

  // Iniciar Sesion 
  app.post("/sessions", users.session);

  app.put("/updateuser/:userId", users.updatexId);
  // Create a new Users
  app.post("/user", users.create);
  // Retrieve all users
  app.get("/users", users.findAll);
  // Retrieve a single Customer with customerId
  app.get("/users/:userId", users.findOne);
  
  // Update a Customer with customerId
  app.put("/users/:userId", users.update);
  // Delete a Customer with customerId
  app.delete("/users/:userId", users.delete);
  // Create a new Customer
  app.delete("/users", users.deleteAll);
};
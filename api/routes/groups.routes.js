module.exports = app => {
    const groups = require('../controllers/groups.controllers') // --> ADDED THIS

    app.post("/groups", groups.create);   // CREAR UN NUEVO GRUPO
    app.get("/groups", groups.findAll);   // BUSCAR 
    app.post("/school_groups", groups.schoolbyId)
    app.get("/groupsxid/:group_id", groups.groupsOne);

    app.put("/groups/:group_id", groups.updateGroups);

    
  };
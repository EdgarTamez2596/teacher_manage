const sql = require("./db.js");

// constructor
const Users = function(user) {
  this.email  = user.email;
  this.name   = user.name;
  this.active = user.active;
};
// VARIABLES PARA QUERYS

Users.login = (loger,result) =>{
  var user_schools = [];
  sql.query("SELECT * FROM users WHERE email = ? AND password = ?", [loger.email, loger.password],(err, res) => {
    if (err) { console.log("error: ", err); 
      result(err, null);
      return 
    }
      if(res[0].id){
        sql.query(`SELECT us.id_school, s.name FROM users u INNER JOIN user_schools us ON u.id = us.id_user
                                                                         INNER JOIN schools  s ON  us.id_school 
                    WHERE u.id = ? AND u.id = us.id_user AND us.id_school = s.id`, [res[0].id] , (err, response)=>{
                      console.log('SEGUNDA RESPUESTA', response)
          if(response.length){          
            for(const i in response){ user_schools.push({ id: response[i].id_school, name: response[i].name }) }
            const payload = { id: res[0].id, 
                            name: res[0].name, 
                            email: res[0].email,
                            password: res[0].password,
                            level: res[0].level,
                            photo: res[0].photo,
                            active: res[0].active,
                            user_schools: user_schools 
                          }
            result(null, payload)
            
          }

        })
      }
    // result({ kind: "not_found" }, null);
  });
};

// ----------------------------------------------------------------------
// ----------------------------------------------------------------------
  Users.updatexId = (id, user, result)=>{
    sql.query("UPDATE users SET name = ?, email = ?, password = ?,level = ?, active=? WHERE id = ?", 
              [user.name, user.email, user.password, user.level, user.active, id], (err, res) => 
      {
        if (err) { 
          console.log("error: ", err);
          result(null, err);
          return;
        }
        if (res.affectedRows == 0) {
          // not found user with the id
          result({ kind: "not_found" }, null);
          return;
        }
        console.log("Se actualizo el user: ", { id: id, ...user });
        result(null, { id: id, ...user });
      }
    );
  };

  Users.deleteSchoolsxUser = (id, result) => {
    sql.query("DELETE FROM user_schools WHERE id_user = ?", id, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      console.log(`deleted ${res.affectedRows} user`);
      result(null, res);
    });
  };

  Users.createSchoolsxUser = (id,school, result) => {
    console.log('crear escuela x usuario');
    sql.query("INSERT INTO user_schools(id_user, id_school)VALUE(?,?)",[id,school], (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }

      // console.log("created user: ", { id: res.insertId, ...newUser });
      // result(null, res);
    });
  };
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------

// --- SIN USAR---->
Users.create = (newUser, result) => {
  console.log('crear un usuario', newUser);
  sql.query("INSERT INTO users SET ?", newUser, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    console.log("created user: ", { id: res.insertId, ...newUser });
    result(null, { id: res.insertId, ...newUser });
  });
};

Users.findById = (userid, result) => {
  console.log("Recibo", userid)
  sql.query(`SELECT * FROM users WHERE id = ${userid}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found user: ", res[0]);
      result(null, res[0]);
      return;
    }
    // not found Customer with the id
    result({ kind: "not_found" }, null);
  });
};

Users.getAll = result => {
  sql.query("SELECT * FROM users", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("users: ", res);
    result(null, res);
  });
};

Users.updateById = (id, user, result) => {
  sql.query("UPDATE users SET email = ?, name = ?, active = ? WHERE id = ?", [user.email, user.name, user.active, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        // not found user with the id
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated user: ", { id: id, ...user });
      result(null, { id: id, ...user });
    }
  );
};

Users.remove = (id, result) => {
  sql.query("DELETE FROM users WHERE id = ?", id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found Customer with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted user with id: ", id);
    result(null, res);
  });
};

Users.removeAll = result => {
  sql.query("DELETE FROM users", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log(`deleted ${res.affectedRows} user`);
    result(null, res);
  });
};

module.exports = Users;
const sql = require("./db.js");

// constructor
const Groups = function(school) {
    this.id         = group.id;
    this.name       = group.name;
    this.user_id    = group.user_id;
    this.school_id  = group.school_id;
    this.start_cycle= group.start_cycle;
    this.end_cycle  = group.end_cycle;
    this.active     = group.active;
};


Groups.schoolxId = (params, result)=>{
	sql.query("SELECT * FROM groups WHERE user_id=? AND school_id=?", [params.user_id, params.school_id], (err,res)=>{
		if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("schoolxID: ", res);
    result(null, res);
	})
};


// ------ FUNCIONANDO--------
// --------------------------

Groups.create = (newGroup, result) => {
	sql.query(`INSERT INTO groups(name,user_id,school_id,start_cycle,end_cycle,active)VALUES(?,?,?,?,?,?)`,
						 [newGroup.name,newGroup.user_id,newGroup.school_id,newGroup.start_cycle, newGroup.end_cycle,newGroup.active], (err, res) => {	
    if (err) {
    console.log("error: ", err);
    result(err, null);
    return;
    }
    // console.log("Crear Grupo: ", res.insertId);
    console.log("Crear Grupo: ", { id: res.insertId, ...newGroup });
    result(null, { id: res.insertId, ...newGroup });
	});
};

Groups.getAll = result => {
  sql.query(`SELECT g.id, g.name as nomgroup, g.user_id, u.name as nomuser, g.school_id, s.name as nomschool, g.start_cycle, g.end_cycle
              FROM teacher_control.groups g LEFT JOIN users u ON g.user_id = u.id
                                            LEFT JOIN schools s ON g.school_id = s.id`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    console.log("groups: ", res);
    result(null, res);
  });
};

Groups.groupsxId = (group_id, result) => {
  sql.query(`SELECT g.id ,g.name, g.user_id, u.name as nomuser, g.school_id, s.name as nomschool, g.start_cycle, g.end_cycle
                FROM groups g LEFT JOIN users u   ON g.user_id = u.id
                              LEFT JOIN schools s ON g.school_id = s.id
              WHERE g.id = ?`,[ group_id ], (err, res) => {
    if (err) {
      console.log("error: ", err); 
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found students: ", res);
      result(null, res);
      return;
    }
    result({ kind: "not_found" }, null);
  });
};

Groups.putGroup = (id, group, result) => {
  sql.query(` UPDATE groups SET name = ?, user_id = ?, school_id = ?, start_cycle = ?, end_cycle = ? , active =?
                WHERE id = ?`, [group.name, group.user_id, group.school_id, group.start_cycle , group.end_cycle, group.active,  id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated group: ", { id: id, ...group });
      result(null, { id: id, ...group });
    }
  );
};


module.exports = Groups;
const sql = require("./db.js");

// constructor
const Schools = function(school) {
    this.id     = school.school;
    this.name   = school.name;
    this.active = school.active;
};

Schools.getAll = result => {
  sql.query("SELECT * FROM schools", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("schools: ", res);
    result(null, res);
  });
};

module.exports = Schools;
const sql = require("./db.js");

// constructor
const Students = function(students) {
  this.name     = studens.name;
  this.paternal = studens.paternal;
  this.maternal = studens.maternal;
  this.age      = studens.age;
  this.dad_name = studens.dad_name;
  this.mom_name = studens.mom_name;
  this.phone    = studens.phone;
  this.emergency_phone = studens.emergency_phone;
  this.group_id = studens.group_id
  this.active   = studens.active;
};
// VARIABLES PARA QUERYS

Students.addStudent = (id,data,result) =>{
  sql.query(`INSERT INTO students (name, paternal, maternal, age , dad_name, mom_name, phone, emergency_phone, group_id, active)
              VALUE(?,?,?,?,?,?,?,?,?,?)` ,[data.name,
                                            data.paternal,
                                            data.maternal,
                                            data.age,
                                            data.dad_name,
                                            data.mom_name,
                                            data.phone,
                                            data.emergency_phone,
                                            id,
                                            1], 
  (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null,err);
      return;
    }
    result(null,res);
  });
};

Students.studentxGroup = (group_id, result) => {
  sql.query(`SELECT * FROM students WHERE group_id = ${group_id}`, (err, res) => {
    if (err) {
      console.log("error: ", err); 
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found students: ", res);
      result(null, res);
      return;
    }
    result({ kind: "not_found" }, null);
  });
};

Students.deleteStudent = (id, result ) =>{
  sql.query(`DELETE FROM students WHERE id = ? ;` ,[ id ], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null,err);
      return;
    }
    result(null,res);
  });
};




module.exports = Students;
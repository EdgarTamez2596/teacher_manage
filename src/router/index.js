import Vue from 'vue'
import Router from 'vue-router'
import Home from '../views/Home.vue'

const Login     = () => import('@/views/AppControl/Login.vue');
const Perfil    = () => import('@/views/AppControl/Perfil.vue');
const Registro  = () => import('@/views/AppControl/Registro.vue');

const CatGrupos  = () => import('@/views/Catalogos/Grupos/CatGrupos.vue');
const NuevoGrupo = () => import( '@/views/Catalogos/Grupos/NuevoGrupo.vue');
const EdiatGrupo = () => import( '@/views/Catalogos/Grupos/EditarGrupo.vue');

const CatEscuelas     = () => import( '@/views/Catalogos/Escuelas/CatEscuelas.vue');
const CatEvaluaciones = () => import( '@/views/Catalogos/Evaluaciones/CatEvaluaciones.vue');
const CatNiveles      = () => import( '@/views/Catalogos/Niveles/CatNiveles.vue');
const CatUsuarios     = () => import( '@/views/Catalogos/Usuarios/CatUsuarios.vue');

const GroupsControl   = () => import( '@/views/UserControl/Groups/GroupsControl.vue');

Vue.use(Router)

const routes = [
  { path: '/home'    ,name: 'home'    , component: Home },
  { path: '/perfil'  ,name: 'perfil'  , component: Perfil },
  { path: '/'        ,name: 'login'   , component: Login },
  { path: '/registro',name: 'registro', component: Registro },

  // USUARIOS
  { path: '/catusuarios'    ,name: 'catusuarios' , component: CatUsuarios },
  // GRUPOS
  { path: '/catgrupos'      ,name: 'catgrupos'   , component: CatGrupos },
  { path: '/nuevogrupo'     ,name: 'nuevogrupo'  , component: NuevoGrupo },
  { path: '/editargrupo/:id',name: 'editargrupo' , component: EdiatGrupo },
  // CONTROL DE GRUPOS
  { path: '/groupscontrol',name: 'groupscontrol' , component: GroupsControl },

  // ESCUELAS
  { path: '/catescuelas'    ,name: 'catescuelas' , component: CatEscuelas },
  // EVALUACIONES
  { path: '/catevaluaciones',name: 'catevaluaciones', component: CatEvaluaciones },
  // NIVELES
  { path: '/catniveles'     ,name: 'catniveles'  , component: CatNiveles },
]

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})


export default router 

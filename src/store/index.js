import Vue from 'vue'
import Vuex from 'vuex'
import axios 		from 'axios'
import VueAxios from 'vue-axios'
import router   from '@/router'

import Login     from '@/modules/Login/Login'
import Perfil    from '@/modules/Perfil/Perfiles'

Vue.use(Vuex)

export default new Vuex.Store({
  modules:{
    Login,
    Perfil,
  },
  
  state: {
    iduser: '',
    logeado: false,
  },
  mutations: {
    setIdUser(state, id){
      state.user = iduser
    },

    setLogeado(state, loger){
      state.logeado = loger 
    }
  },
  actions: {
    saveInfoUser({commit}, payload){
      console.log('payload', payload)

      commit("setIdUser", payload.id)
      commit("setLogeado", true)
    },

    salir({commit}){
      // commit("setToken", '')
      // commit("setUsuario", '')
      // localStorage.removeItem("token")
      router.push({name: 'login'})
    }

  },
  getters:{
		getlogeado(state){
			return state.logeado
		},
		getidUser(state){
			return state.user
		},

	},
})

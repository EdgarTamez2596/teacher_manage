import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import '@mdi/font/css/materialdesignicons.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css' // Ensure you are using css-loader
// import colors from 'vuetify/es5/util/colors'
import es from 'vuetify/es5/locale/es';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
			themes: {
				light: {
					azul: '#01579B', // AZUL
					verde: '#004D40',// VERDE
					rosa: '#bf1c7f',    // ROSA
					cafe: '#6D4C41',   // NEGRO
					morado: '#894975',  // MORADO
					gris: '#6f7170',     // GRIS
					turquesa: "#039BE5"
				},
			},
		},

    lang:{
        locales:{ es },
        current: 'es'
    }
});

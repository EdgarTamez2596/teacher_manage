import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'

export default{
	namespaced: true,
	state:{
		group_select: '',
		school_select: ''
	},

	mutations:{
		GROUP_SELECT(state, group){
			state.group_select = group
		},
		SCHOOL_SELECT(state, school){
			state.school_select = school
		}
	},

	actions:{

		groupSelection({commit}, group){
			commit('GROUP_SELECT',group)
		},

		schoolSelection({commit}, school){
			commit('SCHOOL_SELECT',school)
		},

	},

	getters:{
		getSchoolSelect(state){
			return state.school_select
		},

		getGroupSelect(state){
			return state.group_select
		}
	}
}
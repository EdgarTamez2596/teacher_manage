import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'

export default{
	namespaced: true,

	state:{
		login:true,
		datauser:[],
	},

	mutations:{
		DATA_USER(state, datauser){
			state.datauser = datauser
		},

		NAVBAR(state, payload){
			state.login  = payload 
		},
	},

	actions:{
		validateUser({commit}, session){
			return new Promise((resolve, reject) => {
			  Vue.http.post('sessions', session).then(response=>{
					console.log('user', response.body)
					resolve(true) 
					commit('DATA_USER', response.body)
					commit("NAVBAR", true)
					// this.$store.dispatch("saveInfoUser", response.body)
				}).catch(error =>{
					resolve('error',error)
				})
			})
		},

		ActualizaUser({commit}, id){
			return new Promise((resolve, reject) => {
			  Vue.http.get('users/'+ id).then(response=>{
					resolve(true) 
					commit('DATA_USER', response.body.user)
					// this.$store.dispatch("saveInfoUser", response.body)
				}).catch(error =>{
					resolve(error.body.errors)
				})
			})
		},

		CambiaNavBar({commit}, payload){
			commit("NAVBAR", payload)
		},

	},

	getters:{
		getLogeado(state){
		  return state.login
		},

		getdatauser(state){
			return state.datauser
		},
	}
}